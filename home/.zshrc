# Start configuration added by Zim install {{{
#
# User configuration sourced by interactive shells
#

# -----------------
# Zsh configuration
# -----------------

#
# History
#

# Remove older command from the history if a duplicate is to be added.
setopt HIST_IGNORE_ALL_DUPS

#
# Input/output
#

# Set editor default keymap to emacs (`-e`) or vi (`-v`)
bindkey -e

# Prompt for spelling correction of commands.
# setopt CORRECT

# Customize spelling correction prompt.
#SPROMPT='zsh: correct %F{red}%R%f to %F{green}%r%f [nyae]? '

# Remove path separator from WORDCHARS.
WORDCHARS=${WORDCHARS//[\/]}

# -----------------
# Zim configuration
# -----------------

# Use degit instead of git as the default tool to install and update modules.
zstyle ':zim:zmodule' use 'degit'

# --------------------
# Module configuration
# --------------------

#
# git
#

# Set a custom prefix for the generated aliases. The default prefix is 'G'.
#zstyle ':zim:git' aliases-prefix 'g'

#
# input
#

# Append `../` to your input for each `.` you type after an initial `..`
#zstyle ':zim:input' double-dot-expand yes

#
# termtitle
#

# Set a custom terminal title format using prompt expansion escape sequences.
# See http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html#Simple-Prompt-Escapes
# If none is provided, the default '%n@%m: %~' is used.
#zstyle ':zim:termtitle' format '%1~'

#
# zsh-autosuggestions
#

# Disable automatic widget re-binding on each precmd. This can be set when
# zsh-users/zsh-autosuggestions is the last module in your ~/.zimrc.
ZSH_AUTOSUGGEST_MANUAL_REBIND=1

# Customize the style that the suggestions are shown with.
# See https://github.com/zsh-users/zsh-autosuggestions/blob/master/README.md#suggestion-highlight-style
#ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=242'

#
# zsh-syntax-highlighting
#

# Set what highlighters will be used.
# See https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters.md
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

# Customize the main highlighter styles.
# See https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters/main.md#how-to-tweak-it
#typeset -A ZSH_HIGHLIGHT_STYLES
#ZSH_HIGHLIGHT_STYLES[comment]='fg=242'

# ------------------
# Initialize modules
# ------------------

ZIM_HOME=${ZDOTDIR:-${HOME}}/.zim
# Download zimfw plugin manager if missing.
if [[ ! -e ${ZIM_HOME}/zimfw.zsh ]]; then
  if (( ${+commands[curl]} )); then
    curl -fsSL --create-dirs -o ${ZIM_HOME}/zimfw.zsh \
        https://github.com/zimfw/zimfw/releases/latest/download/zimfw.zsh
  else
    mkdir -p ${ZIM_HOME} && wget -nv -O ${ZIM_HOME}/zimfw.zsh \
        https://github.com/zimfw/zimfw/releases/latest/download/zimfw.zsh
  fi
fi
# Install missing modules, and update ${ZIM_HOME}/init.zsh if missing or outdated.
if [[ ! ${ZIM_HOME}/init.zsh -nt ${ZDOTDIR:-${HOME}}/.zimrc ]]; then
  source ${ZIM_HOME}/zimfw.zsh init -q
fi
# Initialize modules.
source ${ZIM_HOME}/init.zsh
# if [[ ! -f ~/.zpm/zpm.zsh ]]; then
#   git clone --recursive https://github.com/zpm-zsh/zpm ~/.zpm
# fi
# source ~/.zpm/zpm.zsh

# zpm load zpm-zsh/colors,async
# # utilities and stuff
# zpm load \
#   zimfw/environment,async \
#   zimfw/git,async \
#   zimfw/Input,async \
#   zimfw/termtitle,async \
#   zimfw/utility,async \
#   zimfw/duration-info,async \
#   zimfw/utility,async \
#   zimfw/duration-info,async \
#   zpm-zsh/pr-git,async
# # theme
# zpm load @exec/starship,origin:'starship init zsh'
# zpm load z-shell/F-Sy-H
# zpm load zsh-users/zsh-history-substring-search
# # autocomplete
# zpm load \
#   hlissner/zsh-autopair,async \
#   zsh-users/zsh-completions,fpath:src,async \
#   zpm-zsh/zsh-autosuggestions \
#   zimfw/completion,async
# # QoL stuff
# zpm load \
#   zpm-zsh/undollar,async \
#   zpm-zsh/extract,async \
#   zpm-zsh/colorize,async \
#   zpm-zsh/command-not-found,async \
#   zpm-zsh/clipboard,async \
#   zimfw/fzf,async \
#   zimfw/pacman,async \
#   zimfw/archive,async \
#   zimfw/prompt-pwd,async
# # exa!
# zpm load zimfw/exa,async
# ------------------------------
# Post-init module configuration
# ------------------------------

#
# zsh-history-substring-search
#

setopt nohup
zmodload -F zsh/terminfo +p:terminfo
# Bind ^[[A/^[[B manually so up/down works both before and after zle-line-init
for key ('^[[A' '^P' ${terminfo[kcuu1]}) bindkey ${key} history-substring-search-up
for key ('^[[B' '^N' ${terminfo[kcud1]}) bindkey ${key} history-substring-search-down
for key ('k') bindkey -M vicmd ${key} history-substring-search-up
for key ('j') bindkey -M vicmd ${key} history-substring-search-down
# }}} End configuration added by Zim install

# Alias
alias sudo='doas'
alias aurpush='makepkg --printsrcinfo >| .SRCINFO;git commit -a --allow-empty; git push'
alias shapush='updpkgsums; makepkg --printsrcinfo >| .SRCINFO; git commit -a --allow-empty; git push'
# alias update-system="paru -Syu; cd /home/kyle/things/github/Hyprland && git pull --recurse-submodules && make all && doas make installheaders && doas make install && cd /home/kyle/things/github/hy3 && git pull && cmake -DCMAKE_BUILD_TYPE=Debug -B build && cmake --build build"
# alias update-system="sudo pacman -Sy && sudo powerpill -Su && paru -Su && paru -Syua"
alias rm='rmtrash'
alias cat='bat --plain --paging=never'
alias lg='lazygit'
alias record='/home/kyle/record.sh'
alias mpv='env -uMANGOHUD mpv'
alias piano='~/.config/hypr/scripts/piano.sh'
alias tb="nc termbin.com 9999"
# alias up-hyprland="uf /tmp/hypr/$(ls /tmp/hypr -sold | head -2 | tail -1)/hyprland.log"
# alias uc-hyprland="cp /tmp/hypr/$HYPRLAND_INSTANCE_SIGNATURE/hyprland.log /tmp/$HYPRLAND_INSTANCE_SIGNATURE.log; uf /tmp/$HYPRLAND_INSTANCE_SIGNATURE.log ; /usr/bin/rm /tmp/$HYPRLAND_INSTANCE_SIGNATURE.log"
# alias hyprcrash="echo logs:;up-hyprland;echo coredump;coredumpctl info Hyprland -1 2>/dev/null >! /tmp/coredump;uf /tmp/coredump"
alias -g osu-wayland='env SDL_VIDEODRIVER=wayland osu-lazer'
alias bg-spawn="hyprctl dispatch exec"

# Fix input keys
bindkey '^H'      backward-kill-word            # ctrl+bs    delete previous word
bindkey '^[[3;5~' kill-word                     # ctrl+del   delete next word
bindkey '^M' accept-line
bindkey '^]' list-expand
#zstyle ':autocomplete:*' min-delay 0.1  # float 


# eval "$(starship init zsh)"
# eval "$(thefuck --alias)"
eval "$(zoxide init zsh)"


help() {
    "$@" --help 2>&1 | bat --plain --language=help
}
uf_gh() {
  gh gist create "$@" --public
}
uf() {
  if [ -z $1 ];then
      curl -F"file=@-" https://c-v.sh --progress-bar | cat
  else 
      curl -F"file=@$1" https://c-v.sh --progress-bar | cat
  fi
}
uf_0x0() {
  if [ -z $1 ];then
      curl -F"file=@-" https://0x0.st --progress-bar | cat
  else 
      curl -F"file=@$1" https://0x0.st --progress-bar | cat
  fi
}
uf_box() {
  if [ -z $1 ];then
      curl -F "fileToUpload=@-" -F "reqtype=fileupload" -F "userhash=5f2fe6866704a2737031c212b" https://catbox.moe/user/api.php --progress-bar | cat
  else 
      curl -F "fileToUpload=@$1" -F "reqtype=fileupload" -F "userhash=5f2fe6866704a2737031c212b" https://catbox.moe/user/api.php --progress-bar | cat
      # curl -F"file=@$1" https://c-v.sh --progress-bar | cat
  fi
}
function hx(){
  # check if file exists
  if [ -f "$1" ]; then
    OWNER=$(stat -c '%U' $1)
  else
    OWNER=""  
  fi
  
  if [[ "$OWNER" == "root" ]]; then
    echo "Owner is root. Do you want to open the file with doasedit?"
    read ANSWER
    if [[ "$ANSWER" == "y" || "$ANSWER" == "Y" || "$ANSWER" == "yes" ]]; then
      /usr/bin/sudo -E hx $*
    fi
  else
    /usr/bin/hx $*;
  fi
}
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
# __conda_setup="$('/opt/anaconda/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
# if [ $? -eq 0 ]; then
#     eval "$__conda_setup"
# else
#     if [ -f "/opt/anaconda/etc/profile.d/conda.sh" ]; then
#         . "/opt/anaconda/etc/profile.d/conda.sh"
#     else
#         export PATH="/opt/anaconda/bin:$PATH"
#     fi
# fi
# unset __conda_setup
# <<< conda initialize <<<


eval $(thefuck --alias)
