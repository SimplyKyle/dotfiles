export SWWW_TRANSITION=any
export SWWW_TRANSITION_STEP=60
export SWWW_TRANSITION_FPS=255
export SWWW_TRANSITION_BEZIER=0.85,0,0.15,1

nsfw_image=$($HOME/.config/wallpaper/nsfw.sh)
sfw_image=$($HOME/.config/wallpaper/sfw.sh)
convert "$sfw_image"  \( -clone 0 -resize 2560x1440\^ -gravity center -extent 2560x1440 -scale 50% -blur 0x2.5 -resize 200%\)\( -clone 0 -resize 2560x1440 \) -delete 0 -gravity center -compose over -composite - | swww img -o HDMI-A-1 - 
convert "$nsfw_image"  \( -clone 0 -resize 2560x1440\^ -gravity center -extent 2560x1440 -scale 50% -blur 0x2.5 -resize 200%\)\( -clone 0 -resize 2560x1440 \) -delete 0 -gravity center -compose over -composite - | swww img -o DP-2 - 
printf "$nsfw_image\n$sfw_image\n" > $HOME/.config/wallpaper/current_wallpaper
