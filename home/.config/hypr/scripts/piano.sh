#!/bin/bash

if pgrep "CFX"; then
    exit 1;
fi

# launch piano
wine /run/media/kyle/1220e6b9-21cc-46f3-80ea-512090173612/CFX\ Concert\ Grand/CFX\ Concert\ Grand.exe & sleep 2 && hyprctl dispatch movewindowpixel "exact 250 100","cfx concert grand.exe" && sleep 1 && ydotool mousemove -a -x 500 -y 180 && ydotool click 0xC0 && ydotool mousemove -x 80 -y 80 && sleep 0.3 && ydotool mousemove -x 120 -y 0 && sleep 0.3 && ydotool mousemove -x 120 -y 220 && ydotool click 0xC0

prev="$XCURSOR_THEME"

export XCURSOR_THEME="Adwaita"

hyprctl setprop "class:cfx" alphaoverride 1
hyprctl setprop "class:cfx" alphainactiveoverride 1
sleep 0.3

while(true); do 
    FinishedLoading="$(ydotool mousemove -a -x 467 -y 182 && hyprpicker -n -z & ydotool mousemove -a -x 200 -y 182 && sleep 0.3 && ydotool mousemove -a -x 467 -y 182 && sleep 0.3 && ydotool click 0xC0)"
    if [[ "$FinishedLoading" =~ "#CFCABD" ]]; then
        break;
    fi
    sleep 1
done

sleep 1




IsActive="$(ydotool mousemove -a -x 370 -y 135 && sleep 0.5 && ydotool click 0xC0 && sleep 0.3 && ydotool mousemove -x 0 -y 20 && ydotool click 0xC0 && ydotool mousemove -a -x 1421 -y 720 && hyprpicker -n -z & ydotool mousemove -a -x 1621 -y 720 && sleep 0.3 && ydotool mousemove -a -x 1421 -y 720 && sleep 0.3 && ydotool click 0xC0)"

if [ "$IsActive" == "#FDFDFD" ]; then
    ydotool click 0xC0
fi

sleep 0.3
ydotool mousemove -x 125 -y 145 && ydotool click 0xC0 && ydotool mousemove -x 0 -y 70 && sleep 0.3 && ydotool click 0xC0 && ydotool mousemove -x 0 -y 200 && ydotool click 0xC0
export XCURSOR_THEME="$prev"
