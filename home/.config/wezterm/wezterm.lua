local wezterm = require 'wezterm';

return {
    font = wezterm.font("JetBrains Mono", {weight="Bold",}),
    font_size = 13.0,
    default_cursor_style = "BlinkingUnderline",
    animation_fps = 30,
    use_ime = true,
    enable_wayland= true,
}
