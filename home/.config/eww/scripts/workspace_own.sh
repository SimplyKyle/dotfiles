#!/bin/bash

monitor=$1
active_monitor="$(hyprctl monitors -j | jq ".[] | select(.id == $monitor)")"
active_workspace="$(echo $active_monitor | jq '.activeWorkspace.id')"
occupied_workspace=("$(hyprctl clients -j | jq -S "[ .[] | select(.monitor == $monitor) | .workspace.id ] | unique | .[]")")
if [[ ! "${occupied_workspace[*]}" =~ ${active_workspace} ]]; then
  occupied_workspace+=("$active_workspace")
  IFS=$'\n' occupied_workspace=($(sort <<<"${occupied_workspace[*]}"))
  unset IFS
fi
workspaces() {
  if [[ ${1:0:9} == "workspace" ]]; then
    active_monitor="$(hyprctl monitors -j | jq ".[] | select(.id == $monitor)")"
    active_workspace="$(echo $active_monitor | jq '.activeWorkspace.id')"
    occupied_workspace=("$(hyprctl clients -j | jq -S "[ .[] | select(.monitor == $monitor) | .workspace.id ] | unique | .[]")")
    if [[ ! "${occupied_workspace[*]}" =~ ${active_workspace} ]]; then
      occupied_workspace+=("$active_workspace")
      IFS=$'\n' occupied_workspace=($(sort <<<"${occupied_workspace[*]}"))
      unset IFS
    fi
  elif [[ ${1:0:15} == "createworkspace" ]]; then
    active_monitor="$(hyprctl monitors -j | jq ".[] | select(.id == $monitor)")"
    active_workspace="$(echo $active_monitor | jq '.activeWorkspace.id')"
    occupied_workspace=("$(hyprctl clients -j | jq -S "[ .[] | select(.monitor == $monitor) | .workspace.id ] | unique | .[]")")
    if [[ ! "${occupied_workspace[*]}" =~ ${active_workspace} ]]; then
      occupied_workspace+=("$active_workspace")
      IFS=$'\n' occupied_workspace=($(sort <<<"${occupied_workspace[*]}"))
      unset IFS
    fi
  elif [[ ${1:0:16} == "destroyworkspace" ]]; then
    active_monitor="$(hyprctl monitors -j | jq ".[] | select(.id == $monitor)")"
    active_workspace="$(echo $active_monitor | jq '.activeWorkspace.id')"
    occupied_workspace=("$(hyprctl clients -j | jq -S "[ .[] | select(.monitor == $monitor) | .workspace.id ] | unique | .[]")")
    if [[ ! "${occupied_workspace[*]}" =~ ${active_workspace} ]]; then
      occupied_workspace+=("$active_workspace")
      IFS=$'\n' occupied_workspace=($(sort <<<"${occupied_workspace[*]}"))
      unset IFS
    fi
  fi
}
module(){
  if [[ ${1:0:9} == "workspace" ]]; then
    echo -n "(eventbox :onscroll \"echo {} | sed -e 's/up/-1/g' -e 's/down/+1/g' | xargs hyprctl dispatch workspace\""
    echo -n "  (box	:class \"works\"	:orientation \"h\" :spacing 2 :space-evenly \"false\""
    for i in ${occupied_workspace[@]}
    do 
      if [ $i = $active_workspace ]
      then
        echo -n "    (button :onclick \"hyprctl dispatch workspace $i\" :class \"0act\" \"$i\") "
      else
        echo -n "    (button :onclick \"hyprctl dispatch workspace $i\" :class \"0norm\" \"$i\") "
      fi
    done
    echo -n "  )"
    echo ")"
  fi
}

# initial start
workspaces "$event"
module "workspace"

# loop start
socat -u UNIX-CONNECT:/tmp/hypr/"$HYPRLAND_INSTANCE_SIGNATURE"/.socket2.sock - | while read -r event; do 
workspaces "$event"
module "$event"
done
