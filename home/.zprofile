export XDG_CONFIG_HOME="$HOME/.config"
export CURSEFORGE_API_KEY="$2a$10$bL4bIL5pUWqfcO7KQtnMReakwtfHbNKh6v1uTpKlzhwoueEJQnPnm"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
export INPUT_METHOD=fcitx
export SDL_IM_MODULE=fcitx
export GLFW_IM_MODULE=ibus
export PATH=$PATH:/home/kyle/.cargo/bin
export PATH=$PATH:/opt/flutter/bin
export PATH=$PATH:/opt/google-cloud-cli/bin
export CATBOX_USER_HASH=5f2fe6866704a2737031c212b
export PYTHONPATH="/home/kyle/.config/mpv/vs/packages:"
export COPILOT_API_KEY=ghu_2Let3JioxhEgd37byCHR5eUWk4cmiI3DmwKC
